fetch(`https://ajax.test-danit.com/api/swapi/films`)
  .then((data) => data.json())
  .then((res) => {
    // console.log(res);
    res.forEach(({ episodeId, name, openingCrawl, characters, id }) => {
      new Movies(episodeId, name, openingCrawl, id).render();

      // FIRST VARIANT
      characters.forEach((character) => {
        fetch(character)
          .then((res) => res.json())
          .then(({ name }) => {
            const ul = document.querySelector(`.list_${id}`);
            ul.insertAdjacentHTML("beforeend", `<li>${name}</li>`);
          });
      });

      // // SECOND VARIANT
      // const charactersPromises = characters.map(char => fetch(char).then(res => res.json()));
      // Promise.allSettled(charactersPromises).then(res => {
      //   const htmlArray = res.filter(({ status }) => status === "fulfilled").map(({ value }) => `<li>${value.name}</li>`);
      //   console.log(htmlArray)
      //
      //   const ul = document.querySelector(`.list_${id}`);
      //   ul.insertAdjacentHTML("beforeend", htmlArray.join(""));
      // });
    });
  });

class Movies {
  constructor(episodeId, name, openingCrawl, id) {
    this.episodeId = episodeId;
    this.name = name;
    this.openingCrawl = openingCrawl;
    this.id = id;
  }

  render() {
    const container = document.querySelector(".container");
    const movie = `
        <div class = "movie">
        <h2>${this.episodeId}</h2>
        <h3>${this.name}</h3>
        <p>${this.openingCrawl}</p>
        <ul class="list_${this.id}"></ul>
        </div>
        `;
    container.insertAdjacentHTML("beforeend", movie);
  }
}
