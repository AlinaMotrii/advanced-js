document.addEventListener("DOMContentLoaded", function () {
  let btn = document.querySelector(".get-position");
  btn.addEventListener("click", getPosition, { once: true });
});

async function getPosition() {
  createSpinner(document.querySelector(".container"));
  let ip = await fetch("https://api.ipify.org/?format=json")
    .then((response) => response.json())
    .then((data) => data.ip)
    .catch((err) => {
      document.write("Sorry no connection");
      console.error("Something went wrong:", err.message);
    });
  fetch(
    `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`
  )
    .then((response) => response.json())
    .then((data) => {
      renderPosition(
        data,
        "continent",
        "country",
        "regionName",
        "city",
        "district"
      );
      deleteSpinner();
    })
    .catch((err) => {
      console.error("Something went wrong:", err.message);
    });
}

function renderPosition(data, ...items) {
  let container = document.querySelector(".container");
  items.forEach((item) => {
    let box = document.createElement("div");
    box.classList.add("line");
    box.insertAdjacentHTML(
      "afterbegin",
      data[item].length > 0
        ? `<div class="header">${item
            .split("N")
            .join(" n")}</div><div class = "name">${data[item]}</div>`
        : `<div class = "header">${item}</div><div class = "name">no information</div>`
    );
    container.append(box);
  });
}

function createSpinner(parent) {
  const spinner = document.createElement("div");
  spinner.classList.add("lds-dual-ring");
  parent.append(spinner);
}

function deleteSpinner() {
  const spinner = document.querySelector(".lds-dual-ring");
  spinner.remove();
}
