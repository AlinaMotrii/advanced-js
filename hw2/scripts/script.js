// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const list = document.createElement("ul");
const container = document.getElementById("root");

books.forEach((book) => {
  try {
    if (!book.author) {
      throw new Error(`No ${author}`);
    }

    if (!book.name) {
      throw new Error(`No ${name}`);
    }

    if (!book.price) {
      throw new Error(`No ${price}`);
    }
      const item = document.createElement("li");
      const authorContainer = document.createElement("div");
      const nameContainer = document.createElement("div");
      const priceContainer = document.createElement("div");
      authorContainer.innerHTML = book.author;
      nameContainer.innerHTML = book.name;
      priceContainer.innerHTML = book.price;

      list.append(item);
      item.append(authorContainer);
      item.append(nameContainer);
      item.append(priceContainer);
  } catch (error) {
    console.error(error);
  }
});

root.append(list);


