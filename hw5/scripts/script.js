// fetch("https://ajax.test-danit.com/api/json/users", {
//   method: "GET",
// })
//   .then((data) => data.json()) // Декодируем ответ в формате json
//   .then((res) =>
//     res.forEach(({ email, name }) => {
//       new Card(email, name).render();
//       console.log(res);
//     })
//   );

// fetch("https://ajax.test-danit.com/api/json/posts", {
//   method: "GET",
// })
//   .then((data) => data.json()) // Декодируем ответ в формате json
//   .then((res) =>
//     res.forEach(({ title, body }) => {
//       new Card(title, body).render();
//       console.log(res);
//     })
//   );

// const fetchUsers = fetch("https://ajax.test-danit.com/api/json/users");
// const fetchPosts = fetch("https://ajax.test-danit.com/api/json/posts");

// Promise.all([fetchUsers, fetchPosts]).then(values => {
//   return Promise.all(values.map(res => res.json()));
// }).then(([users, posts]) =>
// {
//   console.log(users);
//   console.log(posts);
// });

// const array = [
//   fetch("https://ajax.test-danit.com/api/json/users"),
//   fetch("https://ajax.test-danit.com/api/json/posts"),
// ];

// async function main() {
//   try {
//       const res = await Promise.all(array);
// const data = await Promise.all(
//   res.map((item) => {
//     return item.json();
//   }));
// console.log(data);
//   } catch (e) {
//     console.log('Multiple fetches failed');
//   }

// }
// main();

async function main() {
  try {
    const users = await fetch(
      "https://ajax.test-danit.com/api/json/users"
    ).then((res) => res.json());
    const posts = await fetch(
      "https://ajax.test-danit.com/api/json/posts"
    ).then((res) => res.json());
    console.log(users);
    console.log(posts);
    const userInfo = users.concat(posts);
    console.log(userInfo);

    userInfo.forEach((user) => {
      console.log(user);
      const card = new Card(user.title, user.body, user.name, user.email);
      console.log(card);
      card.render();
    });
  } catch (error) {
    console.log(error);
  }
}
main();

//  Promise.all(array)
//    .then((res) => {
//      Promise.all(
//        res.map((item) => {
//          return item.json();
//        })
//      )
//      .then((data) => console.log(data));
//    })
//    .catch((e) => console.log("Multiple catch failed"));

class Card {
  constructor(title, body, name, email) {
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
  }

  render() {
    const container = document.querySelector(".container");

    container.insertAdjacentHTML(
      "beforeend",
      ` <div class="user">
        <h2>${this.title}</h2>
        <p>${this.body}</p>
        <span>${this.name} </span>
        <a href="mailto: ${this.email}">Email: ${this.email}</a>
        </div>`
    );

    const deleteCard = () => {
      const url = `https://ajax.test-danit.com/api/json/posts/${postId}`;
      fetch(url, {
        method: "DELETE",
      });

      document.getElementById(`${postid}`).remove();
    };

    const btnDelete = document.createElement("button");
    btnDelete.innerText = "DELETE";
    container.append(btnDelete);

    btnDelete.addEventListener("click", deleteCard);
  }
}
