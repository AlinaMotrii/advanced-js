class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const junior = new Programmer("Alina", 31, 900, ["JavaScript", "Java"]);
console.log(junior);
const middle = new Programmer("Harry", 35, 2000,["Python", "C++"]);
console.log(middle);
const senior = new Programmer("John", 28, 3000, ["JavaScript", "Java", "C++"]);
console.log(senior);


